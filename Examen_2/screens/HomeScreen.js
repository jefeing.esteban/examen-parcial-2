import React from 'react';
import { View, Text, StyleSheet, ScrollView, Image } from 'react-native';

const image1 = require('../assets/BALL.jpg');
const gifImage = require('../assets/NFL.jpeg');

const azulOscuro = '#0495a1';

const HomeScreen = () => {
    return (
        <ScrollView style={styles.container}>
            <View style={styles.section}>
                <View style={styles.textContainer}>
                    <Text style={styles.heading}>Welcome to the NFL</Text>
                    <Text style={styles.paragraph}></Text>
                </View>
                <View style={styles.imageContainer}>
                    <Image source={image1} style={styles.image} />
                </View>
            </View>

            <View style={styles.section}>
                <View style={styles.textContainer}>
                    <Text style={styles.heading}>What is the purpose of the application?</Text>
                    <Text style={styles.paragraph}>
                    The objective of the application is to provide an interface for accessing and managing a to-do list (all) related to the NFL league. It makes it easy for users to view different items on the to-do list, including all pending tasks, uncompleted tasks, and tasks already completed, among other options.
                        </Text>
                </View>
                <View style={styles.imageContainer}>
                    <Image source={gifImage} style={styles.gifImage} />
                </View>
            </View>

        </ScrollView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#12dbec',
    },
    section: {
        paddingHorizontal: 20,
        paddingBottom: 30,
        marginBottom: 20,
    },
    textContainer: {
        marginBottom: 20,
    },
    heading: {
        color: '#fff',
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 10,
        textAlign: 'center', // Centrar el texto
    },
    paragraph: {
        color: '#fff',
        fontSize: 16,
        textAlign: 'center', // Centrar el texto
    },
    imageContainer: {
        alignItems: 'center',
        marginBottom: 20,
    },
    image: {
        width: 200,
        height: 200, // Ajusta la altura de la imagen
        borderRadius: 40,
    },
    gifImage: {
        width: 350,
        height: 200,
        borderRadius: 10,
    },
    courseContainer: {
        flexWrap: 'wrap', // Añade envoltura
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginTop: 10, // Agrega margen superior
    },
    course: {
        backgroundColor: azulOscuro,
        borderRadius: 10,
        padding: 20,
        alignItems: 'center',
        justifyContent: 'center',
        width: '48%', // Ajusta el ancho del curso
        marginBottom: 20, // Añade margen inferior para separación
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    courseImage: {        width: 100,
        height: 100, // Ajusta la altura de la imagen
        marginBottom: 10,
        borderRadius: 10,
    },
    courseTitle: {
        color: '#fff',
        fontSize: 16,
        fontWeight: 'bold',
        textAlign: 'center', // Centrar el texto
        marginBottom: 10,
    },
    courseDuration: {
        color: '#fff',
        fontSize: 14,
        textAlign: 'center', // Centrar el texto
    },
});

export default HomeScreen;