import React, { useState, useEffect } from 'react';
import { View, ScrollView } from 'react-native';
import { DataTable } from 'react-native-paper';
import { fetchTodos, mapTodosToIDsAndUserIDs } from './api';

export default function TodosIDsAndUserIDScreen() {
  const [todosIDsAndUserIDs, setTodosIDsAndUserIDs] = useState([]);

  useEffect(() => {
    async function fetchData() {
      try {
        const todos = await fetchTodos();
        const idsAndUserIDs = mapTodosToIDsAndUserIDs(todos);
        setTodosIDsAndUserIDs(idsAndUserIDs);
      } catch (error) {
        console.error('Error fetching todos IDs and userIDs:', error);
      }
    }

    fetchData();
  }, []);

  return (
    <View style={{ flex: 1 }}>
      <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
        <View style={{ backgroundColor: '#f0f0f0', padding: 20 }}>
          <DataTable style={{ borderWidth: 1, borderColor: '#ddd' }}>
            <DataTable.Header>
              <DataTable.Title>ID</DataTable.Title>
              <DataTable.Title>UserID</DataTable.Title>
            </DataTable.Header>
  
            {todosIDsAndUserIDs.map(todo => (
              <DataTable.Row key={todo.id}>
                <DataTable.Cell>{todo.id}</DataTable.Cell>
                <DataTable.Cell>{todo.userId}</DataTable.Cell>
              </DataTable.Row>
            ))}
          </DataTable>
        </View>
      </ScrollView>
    </View>
  );
}
