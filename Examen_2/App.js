import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import TodosIDsScreen from './screens/TodosIDsScreen';
import TodosIDsAndTitlesScreen from './screens/TodosIDsAndTitlesScreen';
import PendingTodosScreen from './screens/PendingTodosScreen';
import CompletedTodosScreen from './screens/CompletedTodosScreen';
import TodosIDsAndUserIDScreen from './screens/TodosIDsAndUserIDScreen';
import CompletedTodosAndUserIDScreen from './screens/CompletedTodosAndUserIDScreen';
import PendingTodosAndUserIDScreen from './screens/PendingTodosAndUserIDScreen';
import HomeScreen from './screens/HomeScreen';

const Drawer = createDrawerNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Drawer.Navigator initialRouteName="Home">
        <Drawer.Screen name="Home" component={HomeScreen} />
        <Drawer.Screen name="Todos los IDs" component={TodosIDsScreen} />
        <Drawer.Screen name="Todos los IDs y Titles" component={TodosIDsAndTitlesScreen} />
        <Drawer.Screen name="Todos los pendientes" component={PendingTodosScreen} />
        <Drawer.Screen name="Todos los completados" component={CompletedTodosScreen} />
        <Drawer.Screen name="Todos los IDs y UserID" component={TodosIDsAndUserIDScreen} />
        <Drawer.Screen name="Todos los completados y UserID" component={CompletedTodosAndUserIDScreen} />
        <Drawer.Screen name="Todos los pendientes y UserID" component={PendingTodosAndUserIDScreen} />
      </Drawer.Navigator>
    </NavigationContainer>
  );
}
